﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.DTOs;
using ApplicationCore.Entities;
using ApplicationCore.Exceptions;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Parameters;
using ApplicationCore.Wrappers;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/cemetery")]
    [ApiController]
    [Authorize]
    public class CemeteryController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public CemeteryController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetPagedCemeteries([FromQuery]CemeteryRequestParameters requestParams)
        {
            var cemeteries = await unitOfWork.Cemeteries.GetPagedCemeteries(requestParams);
            return Ok(cemeteries);
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAllCemeteries()
        {
            var cemeteries = await unitOfWork.Cemeteries.GetAllAsync(includes: x => x.District);
            var mappedCemeteries = mapper.Map<IReadOnlyList<CemeteryDTO>>(cemeteries);

            return Ok(new Response<IReadOnlyList<CemeteryDTO>>(mappedCemeteries));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            var cemetery = await unitOfWork.Cemeteries
                .GetOneAsync(x => x.CemeteryId == id, x => x.District);

            if (cemetery == null)
                throw new ApiException($"'{id}' unikal nömrəli qəbiristanlıq tapılmadı");

            var mappedCemetery = mapper.Map<CemeteryDTO>(cemetery);

            return Ok(new Response<CemeteryDTO>(mappedCemetery));
        }

        [HttpGet("district/{districtId}")]
        public async Task<IActionResult> GetCemeteriesByDistrictId(int districtId)
        {
            var cemeteries = await unitOfWork.Cemeteries
                .GetAllAsync(x => x.DistrictId == districtId, x => x.District);

            if(cemeteries.Count == 0)
                throw new ApiException("Seçilmiş rayonda/şəhərdə qəbiristanlıq mövcud deyil");

            var mappedCemeteries = mapper.Map<IList<CemeteryDTO>>(cemeteries);

            return Ok(new Response<IList<CemeteryDTO>>(mappedCemeteries));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOne(int id)
        {
            if(await unitOfWork.Burials.AnyAsync(x => x.CemeteryId == id))
                throw new ApiException($"Bu qəbiristanlıqda dəfnlər mövcud olduğuna görə silmək olmaz");

            var cemetery = await unitOfWork.Cemeteries.GetOneAsync(x => x.CemeteryId == id);

            await unitOfWork.Cemeteries.DeleteAsync(cemetery);
            return Ok(new Response<string>(null, "Seçilmiş qəbiristanlıq uğurla silindi"));
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CemetryCreateDTO cemeteryDTO)
        {
            var cemetery = mapper.Map<Cemetery>(cemeteryDTO);

            var createdCemetery = await unitOfWork.Cemeteries.AddAsync(cemetery);

            var cemeteryWithDistrict = await unitOfWork.Cemeteries
                .GetOneAsync(x => x.CemeteryId == createdCemetery.CemeteryId, x => x.District);

            var mappedCemetery = mapper.Map<CemeteryDTO>(cemeteryWithDistrict);

            return Ok(new Response<CemeteryDTO>(mappedCemetery, "Qəbiristanlıq uğurla əlavə edildi"));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] CemeteryUpdateDTO cemeteryUpdateDTO)
        {
            var cemetery = mapper.Map<Cemetery>(cemeteryUpdateDTO);

            await unitOfWork.Cemeteries.UpdateAsync(cemetery);

            var cemeteryWithDistrict = await unitOfWork.Cemeteries
                .GetOneAsync(x => x.CemeteryId == cemetery.CemeteryId, x => x.District);

            var mappedCemetery = mapper.Map<CemeteryDTO>(cemeteryWithDistrict);

            return Ok(new Response<CemeteryDTO>(mappedCemetery, "Qəbiristanlıq uğurla redaktə edildi"));
        }
    }
}
