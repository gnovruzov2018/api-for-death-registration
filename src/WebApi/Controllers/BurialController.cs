﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.DTOs;
using ApplicationCore.Entities;
using ApplicationCore.Exceptions;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Parameters;
using ApplicationCore.Wrappers;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Authorize]
    [Route("api/burial")]
    [ApiController]
    public class BurialController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IAuthenticatedUserService authenticatedUser;
        private readonly IUserService userService;
        private readonly IRegistryService registryService;
        private readonly int userId;
        private readonly string roleName;

        public BurialController(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IAuthenticatedUserService authenticatedUser,
            IUserService userService,
            IRegistryService registryService)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.authenticatedUser = authenticatedUser;
            this.userService = userService;
            this.registryService = registryService;

            this.userId = this.authenticatedUser.UserId;
            this.roleName = this.authenticatedUser.RoleName;
        }

        [HttpGet("getpersonbynumber")]
        public async Task<IActionResult> GetPersonByPin([FromQuery] PersonBySeriesAndNumberRequest request)
        {
            var person = await registryService.GetPersonWithNumber(request);

            if (person == null)
                throw new ApiException($"Daxil etdiyiniz '{request.Series} {request.Number}' Ş/V məlumatları yanlışdır");

            return Ok(new Response<PersonFromServiceDTO>(person));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] BurialRequestParameters requestParams)
        {
            List<int> cemeteryIds = new List<int>();
            var user = await userService.GetOne<User>(x => x.UserId == userId, x => x.Role);

            if(user.Role.RoleName == "Supervisor")
                cemeteryIds = await unitOfWork.Cemeteries.GetCemeteryIdsByDistrictId(user.DistrictId.GetValueOrDefault());
            else if(user.Role.RoleName == "Cemetery")
                cemeteryIds.Add(user.CemeteryId.GetValueOrDefault());

            requestParams.CemeteryIds = cemeteryIds;

            var burials = await unitOfWork.Burials.GetPagedBurials(requestParams);
            return Ok(burials);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            var burial = await unitOfWork.Burials
                .GetOneAsync(x => x.BurialId == id, x => x.BuriedPerson, x => x.Cemetery, x => x.DeathApprovalDocument, x => x.RelativePerson);

            if (burial == null)
                throw new ApiException($"Dəfn haqqında məlumat tapılmadı");

            return Ok(new Response<Burial>(burial));
        }

        [HttpPost]
        public async Task<ActionResult> Create(BurialCreateDTO model)
        {
            var buriedPerson = mapper.Map<BuriedPerson>(model.BuriedPerson);
            var deathApprovalDocument = mapper.Map<DeathApprovalDocument>(model.DeathApprovalDocument);
            var relativePerson = mapper.Map<RelativePerson>(model.RelativePerson);

            var burial = new Burial();
            burial.BuriedPerson = buriedPerson;
            burial.DeathApprovalDocument = deathApprovalDocument;
            burial.RelativePerson = relativePerson;
            burial.CemeteryId = model.CemeteryId;
            burial.BurialDate = model.BurialDate;
            burial.GraveNumber = model.GraveNumber;
            burial.RelocationNote = model.RelocationNote;
            burial.UserId = 1; 
            int userid = Convert.ToInt32(authenticatedUser.UserId);

            var createdBurial = await unitOfWork.Burials.AddAsync(burial);

            return Ok(new Response<Burial>(createdBurial, "Dəfn sistemə uğurla əlavə edildi"));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var burial = await unitOfWork.Burials.GetOneAsync(x => x.BurialId == id);

            burial.IsDeleted = true;
            await unitOfWork.Burials.UpdateAsync(burial);

            return Ok(new Response<string>(null, "Seçilmiş dəfn haqqında məlumatlar uğurla silindi"));
        }
    }
}
