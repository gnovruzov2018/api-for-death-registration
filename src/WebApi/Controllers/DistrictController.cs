﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.DTOs;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Wrappers;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/district")]
    [ApiController]
    [Authorize]
    public class DistrictController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public DistrictController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var districts = await unitOfWork.Districts.GetAllAsync();
            var mappedDistricts = mapper.Map<IList<DistrictDTO>>(districts);
            return Ok(new Response<IList<DistrictDTO>>(mappedDistricts, "List of Districts"));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            var district = await unitOfWork.Districts.GetOneAsync(x => x.DistrictId == id);
            var mappedDistrict = mapper.Map<DistrictDTO>(district);
            return Ok(new Response<DistrictDTO>(mappedDistrict));
        }
    }
}
