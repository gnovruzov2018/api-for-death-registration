﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.DTOs;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Parameters;
using ApplicationCore.Wrappers;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/statistics")]
    [ApiController]
    public class StatisticsController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public StatisticsController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet("burialsbyyear")]
        public async Task<IActionResult> GetBurialsWithGenderByYear([FromQuery] BurialsByYearRequestParameters requestParameters)
        {
            var data = await unitOfWork.Statistics.GetBurialsWithGenderByYear(requestParameters);

            return Ok(new Response<IReadOnlyList<StatisticsByYearDTO>>(data));
        }

        [HttpGet("burialsbycemeteryandyear")]
        public async Task<IActionResult> GetBurialsWithGenderByCemeteryAndYear([FromQuery] BurialsByCemeteryAndYearRequestParameters requestParameters)
        {
            var data = await unitOfWork.Statistics.GetBurialsWithGenderByCemeteryAndYear(requestParameters);

            return Ok(new Response<IReadOnlyList<StatisticsByYearDTO>>(data));
        }
    }
}
