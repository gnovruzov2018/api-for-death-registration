﻿using ApplicationCore.DTOs;
using ApplicationCore.Entities;
using ApplicationCore.Enums;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            MapCemeteryObjects();
            MapDistrictObjects();
            MapBurialObjects();
        }

        private void MapCemeteryObjects()
        {
            CreateMap<Cemetery, CemeteryDTO>()
                .ForMember(dest => dest.DistrictName, act => act.MapFrom(src => src.District.DistrictName));

            CreateMap<CemetryCreateDTO, Cemetery>();
            CreateMap<CemeteryUpdateDTO, Cemetery>();
        }

        private void MapDistrictObjects()
        {
            CreateMap<District, DistrictDTO>();
        }

        private void MapBurialObjects()
        {
            CreateMap<BuriedPersonDTO, BuriedPerson>()
                .ForMember(dest => dest.Gender, act => act.MapFrom(src => (Gender)src.Gender));
            CreateMap<RelativePersonDTO, RelativePerson>();
            CreateMap<DeathApprovalDocumentDTO, DeathApprovalDocument>();
            CreateMap<Burial, BurialDTO>()
                .ForMember(dest => dest.BuriedPersonFirstName, act => act.MapFrom(src => src.BuriedPerson.FirstName))
                .ForMember(dest => dest.BuriedPersonLastName, act => act.MapFrom(src => src.BuriedPerson.LastName))
                .ForMember(dest => dest.BuriedPersonMiddleName, act => act.MapFrom(src => src.BuriedPerson.MiddleName))
                .ForMember(dest => dest.BirthDate, act => act.MapFrom(src => src.BuriedPerson.BirthDate))
                .ForMember(dest => dest.BurialDate, act => act.MapFrom(src => src.BurialDate))
                .ForMember(dest => dest.CemeteryName, act => act.MapFrom(src => src.Cemetery.CemeteryName));
        }
    }
}
