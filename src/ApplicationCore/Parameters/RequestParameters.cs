﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace ApplicationCore.Parameters
{
    public class RequestParameters
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public RequestParameters()
        {
            this.PageNumber = 1;
            this.PageSize = 10;
        }
        public RequestParameters(int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 10 ? 10 : pageSize;
        }
    }

    public class CemeteryRequestParameters : RequestParameters
    {
        public string SearchText { get; set; }
    }

    public class BurialRequestParameters : RequestParameters
    {
        public string PIN { get; set; }
        public string IdSeries { get; set; }
        public string IdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? BirthDateFrom { get; set; }
        public DateTime? BirthDateTo { get; set; }
        public DateTime? DeathDateFrom { get; set; }
        public DateTime? DeathDateTo { get; set; }
        public DateTime? BurialDateFrom { get; set; }
        public DateTime? BurialDateTo { get; set; }
        public int? CemeteryId { get; set; }

        [JsonIgnore]
        public List<int> CemeteryIds { get; set; }
    }

    public class BurialsByYearRequestParameters
    {
        public int? Year { get; set; }
    }

    public class BurialsByCemeteryAndYearRequestParameters
    {
        public int CemeteryId { get; set; }
        public int? Year { get; set; }
    }
}
