﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Parameters
{
    public class PersonBySeriesAndNumberRequest
    {
        public string Series { get; set; }
        public string Number { get; set; }
    }

    public class PersonByPinRequest
    {
        public string Pin { get; set; }
    }
}
