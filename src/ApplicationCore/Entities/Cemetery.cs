﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace ApplicationCore.Entities
{
    public class Cemetery
    {
        [Key]
        public int CemeteryId { get; set; }
        public string RegistryNumber { get; set; }
        public string CemeteryName { get; set; }
        public string Address { get; set; }

        [ForeignKey("DistrictId")]
        public District District { get; set; }
        public int DistrictId { get; set; }

        [JsonIgnore]
        public ICollection<Burial> Burials { get; set; }
    }
}
