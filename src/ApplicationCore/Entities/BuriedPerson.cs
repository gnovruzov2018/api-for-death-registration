﻿using ApplicationCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationCore.Entities
{
    public class BuriedPerson : BasePerson
    {
        [Key]
        public int BuriedPersonId { get; set; }
        public DateTime BirthDate { get; set; }
        public string PIN { get; set; }
        public string IdSeries { get; set; }
        public string IdNumber { get; set; }
        public Gender Gender { get; set; }
        public DateTime DeathDate { get; set; }
    }
}
