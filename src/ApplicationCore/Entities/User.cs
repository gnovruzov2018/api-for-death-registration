﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ApplicationCore.Entities
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string OfficeName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        [ForeignKey("DistrictId")]
        public District District { get; set; }
        public int? DistrictId { get; set; }


        [ForeignKey("CemeteryId")]
        public Cemetery Cemetery { get; set; }
        public int? CemeteryId { get; set; }

        [ForeignKey("RoleId")]
        public Role Role { get; set; }
        public int RoleId { get; set; }

        public ICollection<Burial> Burials { get; set; }
        public ICollection<RefreshToken> RefreshTokens { get; set; }
    }
}
