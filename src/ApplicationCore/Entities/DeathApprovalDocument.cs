﻿using ApplicationCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationCore.Entities
{
    public class DeathApprovalDocument
    {
        [Key]
        public int DeathApprovalDocumentId { get; set; }
        public string IssuerFirstName { get; set; }
        public string IssuerLastName { get; set; }
        public string IssuerOrganization { get; set; }
        public string IssuerPersonPosition { get; set; }
        public string DocumentSeries { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime DocumentIssueDate { get; set; }
        public string DeathPlace { get; set; }
        public bool DiedAbroad { get; set; }
        public DeathApprovalDocumentType DocumentType { get; set; }
    }
}
