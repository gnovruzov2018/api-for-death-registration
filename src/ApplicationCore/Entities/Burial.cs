﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Burial
    {
        [Key]
        public int BurialId { get; set; }

        [ForeignKey("BuriedPersonId")]
        public BuriedPerson BuriedPerson { get; set; }
        public int BuriedPersonId { get; set; }

        [ForeignKey("DeathApprovalDocumentId")]
        public DeathApprovalDocument DeathApprovalDocument { get; set; }
        public int DeathApprovalDocumentId { get; set; }

        [ForeignKey("CemeteryId")]
        public Cemetery Cemetery { get; set; }
        public int CemeteryId { get; set; }

        [ForeignKey("RelativePersonId")]
        public RelativePerson RelativePerson { get; set; }
        public int RelativePersonId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
        public int UserId { get; set; }

        public string RelocationNote { get; set; }
        public string GraveNumber { get; set; }

        public DateTime BurialDate { get; set; }
        public DateTime CreateDate { get; set; }

        public bool IsDeleted { get; set; }

        public Burial()
        {
            CreateDate = DateTime.Now;
        }

    }
}
