﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.DTOs
{
    public class DistrictDTO
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string DistrictCode { get; set; }
        public int ViewOrder { get; set; }
    }
}
