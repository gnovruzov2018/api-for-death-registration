﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.DTOs.Account
{
    public class RevokeTokenRequest
    {
        public string RefreshToken { get; set; }
    }
}
