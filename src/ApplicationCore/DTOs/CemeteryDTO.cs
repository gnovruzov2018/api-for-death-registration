﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.DTOs
{
    public class CemeteryDTO
    {
        public int CemeteryId { get; set; }
        public string RegistryNumber { get; set; }
        public string CemeteryName { get; set; }
        public string Address { get; set; }
        public string DistrictName { get; set; }
        public int DistrictId { get; set; }
    }
}
