﻿using ApplicationCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.DTOs
{
    public class BurialCreateDTO
    {
        public BuriedPersonDTO BuriedPerson { get; set; }
        public DeathApprovalDocumentDTO DeathApprovalDocument { get; set; }
        public RelativePersonDTO RelativePerson { get; set; }
        public int CemeteryId { get; set; }
        public string RelocationNote { get; set; }
        public string GraveNumber { get; set; }
        public DateTime BurialDate { get; set; }
    }
    
    public class BuriedPersonDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime BirthDate { get; set; }
        public string PIN { get; set; }
        public string IdSeries { get; set; }
        public string IdNumber { get; set; }
        public int Gender { get; set; }
        public DateTime DeathDate { get; set; }

    }

    public class DeathApprovalDocumentDTO
    {
        public string IssuerFirstName { get; set; }
        public string IssuerLastName { get; set; }
        public string IssuerOrganization { get; set; }
        public string IssuerPersonPosition { get; set; }
        public string DocumentSeries { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime DocumentIssueDate { get; set; }
        public string DeathPlace { get; set; }
        public bool DiedAbroad { get; set; }
        public DeathApprovalDocumentType DocumentType { get; set; }
    }

    public class RelativePersonDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PIN { get; set; }
        public string Email { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Address { get; set; }
    }
}
