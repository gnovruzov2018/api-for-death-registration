﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.DTOs
{
    public class StatisticsByYearDTO
    {
        public int Gender { get; set; }
        public int Count { get; set; }
        public int Year { get; set; }
    }

    public class StatisticsByCemeteryAndYearDTO
    {
        public int Gender { get; set; }
        public int Count { get; set; }
        public int Year { get; set; }
        public string CemeteryName { get; set; }
    }
}
