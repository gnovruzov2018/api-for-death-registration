﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.DTOs
{
    public class CemetryCreateDTO
    {
        public string RegistryNumber { get; set; }
        public string CemeteryName { get; set; }
        public string Address { get; set; }
        public int DistrictId { get; set; }
    }
}
