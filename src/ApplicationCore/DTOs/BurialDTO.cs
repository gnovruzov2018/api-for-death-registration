﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.DTOs
{
    public class BurialDTO
    {
        public int BurialId { get; set; }
        public string BuriedPersonFirstName { get; set; }
        public string BuriedPersonLastName { get; set; }
        public string BuriedPersonMiddleName { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime BurialDate { get; set; }
        public string CemeteryName { get; set; }
    }
}
