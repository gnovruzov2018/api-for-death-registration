﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.DTOs
{
    public class PersonFromServiceDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Pin { get; set; }
        public DateTime DeathDate { get; set; }
        public int Gender { get; set; }
    }
}
