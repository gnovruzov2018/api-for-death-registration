﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Interfaces
{
    public interface IAuthenticatedUserService
    {
        int UserId { get; }
        string RoleName { get; }
    }
}
