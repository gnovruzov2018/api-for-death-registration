﻿using ApplicationCore.DTOs;
using ApplicationCore.Parameters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IRegistryService
    {
        public Task GetPersonByPinAsync(PersonByPinRequest request);
        public Task<PersonFromServiceDTO> GetPersonWithNumber(PersonBySeriesAndNumberRequest request);
    }
}
