﻿using ApplicationCore.DTOs;
using ApplicationCore.Entities;
using ApplicationCore.Parameters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces.Repositories
{
    public interface IStatisticsRepository : IGenericRepositoryAsync<Burial>
    {
        Task<IReadOnlyList<StatisticsByYearDTO>> GetBurialsWithGenderByYear(BurialsByYearRequestParameters requestParameters);
        Task<IReadOnlyList<StatisticsByYearDTO>> GetBurialsWithGenderByCemeteryAndYear(BurialsByCemeteryAndYearRequestParameters requestParameters);
    }
}
