﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        ICemeteryRepository Cemeteries { get; }
        IDistrictRepository Districts { get; }
        IBurialRepository Burials { get; }
        IStatisticsRepository Statistics { get; }

        Task SaveAllAsync();
    }
}
