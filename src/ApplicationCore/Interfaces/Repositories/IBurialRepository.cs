﻿using ApplicationCore.DTOs;
using ApplicationCore.Entities;
using ApplicationCore.Parameters;
using ApplicationCore.Wrappers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces.Repositories
{
    public interface IBurialRepository : IGenericRepositoryAsync<Burial>
    {
        Task<Response<PagedResponse<BurialDTO>>> GetPagedBurials(BurialRequestParameters requestParams);
    }
}
