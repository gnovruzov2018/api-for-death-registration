﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Enums
{
    public enum DeathApprovalDocumentType
    {
        IssuedByDoctor = 1,
        IssuedByParamedic = 2,
        IssuedByRegistry = 3
    }
}
