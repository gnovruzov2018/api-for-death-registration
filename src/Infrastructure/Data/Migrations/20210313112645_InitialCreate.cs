﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BuriedPersons",
                columns: table => new
                {
                    BuriedPersonId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    BirthDateStr = table.Column<string>(nullable: true),
                    PIN = table.Column<string>(nullable: true),
                    IdSeries = table.Column<string>(nullable: true),
                    IdNumber = table.Column<string>(nullable: true),
                    DeathDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuriedPersons", x => x.BuriedPersonId);
                });

            migrationBuilder.CreateTable(
                name: "DeathApprovalDocuments",
                columns: table => new
                {
                    DeathApprovalDocumentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IssuerFirstName = table.Column<string>(nullable: true),
                    IssuerLastName = table.Column<string>(nullable: true),
                    IssuerOrganization = table.Column<string>(nullable: true),
                    IssuerPersonPosition = table.Column<string>(nullable: true),
                    DocumentSeries = table.Column<string>(nullable: true),
                    DocumentNumber = table.Column<string>(nullable: true),
                    DocumentIssueDate = table.Column<DateTime>(nullable: false),
                    DeathPlace = table.Column<string>(nullable: true),
                    DiedAbroad = table.Column<bool>(nullable: false),
                    DocumentType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeathApprovalDocuments", x => x.DeathApprovalDocumentId);
                });

            migrationBuilder.CreateTable(
                name: "Districts",
                columns: table => new
                {
                    DistrictId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DistrictName = table.Column<string>(nullable: true),
                    DistrictCode = table.Column<string>(nullable: true),
                    ViewOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Districts", x => x.DistrictId);
                });

            migrationBuilder.CreateTable(
                name: "RelativePersons",
                columns: table => new
                {
                    RelativePersonId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    PIN = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone1 = table.Column<string>(nullable: true),
                    Phone2 = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RelativePersons", x => x.RelativePersonId);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    RoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "Cemeteries",
                columns: table => new
                {
                    CemeteryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RegistryNumber = table.Column<string>(nullable: true),
                    CemeteryName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    DistrictId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cemeteries", x => x.CemeteryId);
                    table.ForeignKey(
                        name: "FK_Cemeteries_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "DistrictId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    OfficeName = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    DistrictId = table.Column<int>(nullable: false),
                    CemeteryId = table.Column<int>(nullable: true),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Users_Cemeteries_CemeteryId",
                        column: x => x.CemeteryId,
                        principalTable: "Cemeteries",
                        principalColumn: "CemeteryId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "DistrictId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Users_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Burials",
                columns: table => new
                {
                    BurialId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BuriedPersonId = table.Column<int>(nullable: false),
                    DeathApprovalDocumentId = table.Column<int>(nullable: false),
                    CemeteryId = table.Column<int>(nullable: false),
                    RelativePersonId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RelocationNote = table.Column<string>(nullable: true),
                    GraveNumber = table.Column<string>(nullable: true),
                    BurialDate = table.Column<DateTime>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Burials", x => x.BurialId);
                    table.ForeignKey(
                        name: "FK_Burials_BuriedPersons_BuriedPersonId",
                        column: x => x.BuriedPersonId,
                        principalTable: "BuriedPersons",
                        principalColumn: "BuriedPersonId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Burials_Cemeteries_CemeteryId",
                        column: x => x.CemeteryId,
                        principalTable: "Cemeteries",
                        principalColumn: "CemeteryId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Burials_DeathApprovalDocuments_DeathApprovalDocumentId",
                        column: x => x.DeathApprovalDocumentId,
                        principalTable: "DeathApprovalDocuments",
                        principalColumn: "DeathApprovalDocumentId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Burials_RelativePersons_RelativePersonId",
                        column: x => x.RelativePersonId,
                        principalTable: "RelativePersons",
                        principalColumn: "RelativePersonId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Burials_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Burials_BuriedPersonId",
                table: "Burials",
                column: "BuriedPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Burials_CemeteryId",
                table: "Burials",
                column: "CemeteryId");

            migrationBuilder.CreateIndex(
                name: "IX_Burials_DeathApprovalDocumentId",
                table: "Burials",
                column: "DeathApprovalDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_Burials_RelativePersonId",
                table: "Burials",
                column: "RelativePersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Burials_UserId",
                table: "Burials",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Cemeteries_DistrictId",
                table: "Cemeteries",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_CemeteryId",
                table: "Users",
                column: "CemeteryId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_DistrictId",
                table: "Users",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleId",
                table: "Users",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Burials");

            migrationBuilder.DropTable(
                name: "BuriedPersons");

            migrationBuilder.DropTable(
                name: "DeathApprovalDocuments");

            migrationBuilder.DropTable(
                name: "RelativePersons");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Cemeteries");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Districts");
        }
    }
}
