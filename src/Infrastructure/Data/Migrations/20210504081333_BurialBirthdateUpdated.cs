﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Data.Migrations
{
    public partial class BurialBirthdateUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BirthDateStr",
                table: "BuriedPersons");

            migrationBuilder.AddColumn<DateTime>(
                name: "BirthDate",
                table: "BuriedPersons",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BirthDate",
                table: "BuriedPersons");

            migrationBuilder.AddColumn<string>(
                name: "BirthDateStr",
                table: "BuriedPersons",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
