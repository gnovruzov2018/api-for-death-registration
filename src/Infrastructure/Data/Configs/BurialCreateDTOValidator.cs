﻿using ApplicationCore.DTOs;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data.Configs
{
    public class BurialCreateDTOValidator : AbstractValidator<BurialCreateDTO>
    {
        public BurialCreateDTOValidator()
        {
            RuleFor(x => x.BuriedPerson.FirstName).NotNull().NotEmpty()
                .WithMessage("Dəfn olunan şəxsin adı boş qoyula bilməz");
            RuleFor(x => x.BuriedPerson.LastName).NotNull().NotEmpty()
                .WithMessage("Dəfn olunan şəxsin soyadı boş qoyula bilməz");
            RuleFor(x => x.BuriedPerson.MiddleName).NotNull().NotEmpty()
                .WithMessage("Dəfn olunan şəxsin ata adı boş qoyula bilməz");
            RuleFor(x => x.BuriedPerson.BirthDate).NotNull().NotEmpty()
                .WithMessage("Dəfn olunan şəxsin doğum tarixi boş qoyula bilməz");
            RuleFor(x => x.BuriedPerson.IdNumber).NotNull().NotEmpty()
                .WithMessage("Dəfn olunan şəxsin Ş/V nömrəsi boş qoyula bilməz");
            RuleFor(x => x.BuriedPerson.IdSeries).NotNull().NotEmpty()
                .WithMessage("Dəfn olunan şəxsin Ş/V seriyası boş qoyula bilməz");
            RuleFor(x => x.BuriedPerson.PIN).NotNull().NotEmpty()
                .WithMessage("Dəfn olunan şəxsin FİN kodu boş qoyula bilməz")
                .Length(5, 7).WithMessage("Dəfn olunan şəxsin FİN kodunu düzgün daxil edin");
            RuleFor(x => x.BuriedPerson.DeathDate).NotNull().NotEmpty()
                .WithMessage("Dəfn olunan şəxsin ölüm boş qoyula bilməz");

            RuleFor(x => x.DeathApprovalDocument.IssuerFirstName).NotNull().NotEmpty()
                .WithMessage("Həkimin adı boş qoyula bilməz");
            RuleFor(x => x.DeathApprovalDocument.IssuerLastName).NotNull().NotEmpty()
                .WithMessage("Həkimin soyadı boş qoyula bilməz");
            RuleFor(x => x.DeathApprovalDocument.DeathPlace).NotNull().NotEmpty()
                .WithMessage("Dəfn olunan şəxsin öldüyü yer boş qoyula bilməz");
            RuleFor(x => x.DeathApprovalDocument.DocumentType).NotNull().NotEmpty()
                .WithMessage("Ölüm faktını təsvir edən sənədin növü boş qoyula bilməz");
            RuleFor(x => x.DeathApprovalDocument.DocumentIssueDate).NotNull().NotEmpty()
                .WithMessage("Ölüm faktını təsvir edən sənədin tərtib edildiyi tarix boş qoyula bilməz"); 
            RuleFor(x => x.DeathApprovalDocument.DocumentNumber).NotNull().NotEmpty()
                 .WithMessage("Ölüm faktını təsvir edən sənədin nömrəsi boş qoyula bilməz");

            RuleFor(x => x.RelativePerson.PIN).NotNull().NotEmpty()
                 .WithMessage("Yaxın şəxsin FİN kodu boş qoyula bilməz");
            RuleFor(x => x.RelativePerson.FirstName).NotNull().NotEmpty()
                 .WithMessage("Yaxın şəxsin adı boş qoyula bilməz");
            RuleFor(x => x.RelativePerson.LastName).NotNull().NotEmpty()
                 .WithMessage("Yaxın şəxsin soyadı boş qoyula bilməz");
            RuleFor(x => x.RelativePerson.MiddleName).NotNull().NotEmpty()
                 .WithMessage("Yaxın şəxsin ata adı boş qoyula bilməz");
            RuleFor(x => x.RelativePerson.Phone1).NotNull().NotEmpty()
                 .WithMessage("Yaxın şəxsə aid ən azı bir əlaqə nömrəsi əlavə edilməlidir");

            RuleFor(x => x.GraveNumber).NotNull().NotEmpty()
                 .WithMessage("Qəbir nömrəsi boş qoyula bilməz");
            RuleFor(x => x.BurialDate).NotNull().NotEmpty()
                 .WithMessage("Dəfn tarixi boş qoyula bilməz");

        }
    }
}
