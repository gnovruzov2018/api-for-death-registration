﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.Seeds
{
    public static class DefaultUsers
    {
        public static async Task SeedAsync(IUserService userService)
        {
            var adminUser = new User
            {
                FirstName = "Qadir",
                LastName = "Novruzov",
                MiddleName = "Gündüz",
                Email = "gadirnovruzov@gmail.com",
                RoleId = 1,
                UserName = "gnovruzov",
                OfficeName = "Super Admin",
                Password = "Puwu69!7",
            };

            if (!(await userService.UserExistsAsync(adminUser.UserName)))
                await userService.AddUserRelatedEntity<User>(adminUser);

            var supervisorUser = new User
            {
                FirstName = "Rasim",
                LastName = "Məmmədov",
                MiddleName = "Fuad",
                Email = "supervisor@gmail.com",
                RoleId = 2,
                UserName = "supervisor",
                OfficeName = "Abşeron rayon icra nümayəndəliyi",
                Password = "Puwu69!7",
                DistrictId = 1
            };

            if (!(await userService.UserExistsAsync(supervisorUser.UserName)))
                await userService.AddUserRelatedEntity<User>(supervisorUser);

            var cemeteryUser = new User
            {
                FirstName = "Tural",
                LastName = "Babayev",
                MiddleName = "Elməddin",
                Email = "tural.asd121@gmail.com",
                RoleId = 3,
                UserName = "cemetery1",
                OfficeName = "Abşeron rayon qəbiristanlığı",
                Password = "Puwu69!7",
                DistrictId = 1,
                CemeteryId = 1
            };

            if (!(await userService.UserExistsAsync(cemeteryUser.UserName)))
                await userService.AddUserRelatedEntity<User>(cemeteryUser);
        }
    }
}
