﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.Seeds
{
    public static class DefaultRoles
    {
        public static async Task SeedAsync(IUserService userService)
        {
            //Seed Roles
            if(!(await userService.RoleExistsAsync("Admin")))
                await userService.AddUserRelatedEntity<Role>(new Role { RoleName = "Admin" });
            if (!(await userService.RoleExistsAsync("Supervisor")))
                await userService.AddUserRelatedEntity<Role>(new Role { RoleName = "Supervisor" });
            if (!(await userService.RoleExistsAsync("Cemetery")))
                await userService.AddUserRelatedEntity<Role>(new Role { RoleName = "Cemetery" });

        }
    }
}
