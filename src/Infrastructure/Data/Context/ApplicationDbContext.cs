﻿using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        
        }

        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<RelativePerson> RelativePersons { get; set; }
        public DbSet<DeathApprovalDocument> DeathApprovalDocuments { get; set; }
        public DbSet<Cemetery> Cemeteries { get; set; }
        public DbSet<BuriedPerson> BuriedPersons { get; set; }
        public DbSet<Burial> Burials { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Burial>()
                .HasOne(x => x.User)
                .WithMany(x => x.Burials)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
