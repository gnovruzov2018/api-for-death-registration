﻿using ApplicationCore.DTOs;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Parameters;
using ApplicationCore.Wrappers;
using AutoMapper;
using Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class CemeteryRepository : GenericRepositoryAsync<Cemetery>, ICemeteryRepository
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public CemeteryRepository(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<List<int>> GetCemeteryIdsByDistrictId(int districtId)
        {
            var cemeteryIds = await context.Cemeteries
                .Where(x => x.DistrictId == districtId).Select(x => x.CemeteryId).ToListAsync();

            return cemeteryIds;
        }

        public async Task<Response<PagedResponse<CemeteryDTO>>> GetPagedCemeteries(CemeteryRequestParameters requestParams)
        {
            var query = context.Cemeteries.AsQueryable();

            if (!string.IsNullOrEmpty(requestParams.SearchText))
                query = query.Where(x => x.CemeteryName.Contains(requestParams.SearchText)
                                            || x.District.DistrictName.Contains(requestParams.SearchText)
                                            || x.Address.Contains(requestParams.SearchText));

            var count = await query.AsNoTracking().CountAsync();

            var cemeteries = await query.Skip((requestParams.PageNumber - 1) * requestParams.PageSize)
                .Take(requestParams.PageSize)
                .Include(x => x.District)
                .AsNoTracking()
                .ToListAsync();

            var mappedCemeteries = mapper.Map<IList<CemeteryDTO>>(cemeteries);

            var pagedResponse = new PagedResponse<CemeteryDTO>
            {
                TotalCount = count,
                Data = mappedCemeteries,
                PageNumber = requestParams.PageNumber,
                PageSize = requestParams.PageSize
            };

            return new Response<PagedResponse<CemeteryDTO>>(pagedResponse, "Qəbiristanlıqların siyahısı");
        }
    }
}
