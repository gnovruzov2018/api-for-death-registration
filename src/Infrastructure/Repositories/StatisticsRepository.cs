﻿using ApplicationCore.DTOs;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Parameters;
using AutoMapper;
using Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class StatisticsRepository : GenericRepositoryAsync<Burial>, IStatisticsRepository
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public StatisticsRepository(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<IReadOnlyList<StatisticsByYearDTO>> GetBurialsWithGenderByCemeteryAndYear(BurialsByCemeteryAndYearRequestParameters requestParameters)
        {
            List<StatisticsByYearDTO> data = new List<StatisticsByYearDTO>();

            var query = context.Burials.AsQueryable();

            query = context.Burials.Where(x => x.CemeteryId == requestParameters.CemeteryId);

            if (requestParameters.Year != null)
                query = query.Where(x => x.BurialDate.Year == requestParameters.Year);

            data = await query.GroupBy(x => new { x.BurialDate.Year, x.BuriedPerson.Gender })
                    .Select(x => new StatisticsByYearDTO
                    {
                        Year = x.Key.Year,
                        Gender = (int)x.Key.Gender,
                        Count = x.Count()
                    }).ToListAsync();

            return data;
        }

        public async Task<IReadOnlyList<StatisticsByYearDTO>> GetBurialsWithGenderByYear(BurialsByYearRequestParameters requestParameters)
        {
            List<StatisticsByYearDTO> data = new List<StatisticsByYearDTO>();

            var query = context.Burials.AsQueryable();

            if (requestParameters.Year != null)
                query = query.Where(x => x.BurialDate.Year == requestParameters.Year);

            data = await query.GroupBy(x => new { x.BurialDate.Year, x.BuriedPerson.Gender })
                    .Select(x => new StatisticsByYearDTO
                    {
                        Year = x.Key.Year,
                        Gender = (int)x.Key.Gender,
                        Count = x.Count()
                    }).ToListAsync();

            return data;
        }
    }
}
