﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories;
using AutoMapper;
using Infrastructure.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Repositories
{
    public class DistrictRepository : GenericRepositoryAsync<District>, IDistrictRepository
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public DistrictRepository(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            this.context = context;
            this.mapper = mapper;
        }
    }
}
