﻿using ApplicationCore.DTOs;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories;
using ApplicationCore.Parameters;
using ApplicationCore.Wrappers;
using AutoMapper;
using Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class BurialRepository : GenericRepositoryAsync<Burial>, IBurialRepository
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public BurialRepository(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<Response<PagedResponse<BurialDTO>>> GetPagedBurials(BurialRequestParameters requestParams)
        {
            var query = context.Burials.Where(x => x.IsDeleted == false).AsQueryable();

            if (requestParams.CemeteryIds.Count > 0)
                query = query.Where(x => requestParams.CemeteryIds.Contains(x.CemeteryId));

            if(requestParams.IdNumber != null)
            {
                query = query.Where(x => x.BuriedPerson.IdSeries == requestParams.IdSeries && x.BuriedPerson.IdNumber == requestParams.IdNumber);
            }
            else if(requestParams.PIN != null)
            {
                query = query.Where(x => x.BuriedPerson.PIN == requestParams.PIN);
            }
            else
            {
                if (requestParams.FirstName != null)
                    query = query.Where(x => x.BuriedPerson.FirstName.ToLower().StartsWith(requestParams.FirstName.ToLower()));
                if (requestParams.LastName != null)
                    query = query.Where(x => x.BuriedPerson.LastName.ToLower().StartsWith(requestParams.LastName.ToLower()));
                if (requestParams.MiddleName != null)
                    query = query.Where(x => x.BuriedPerson.MiddleName.ToLower().StartsWith(requestParams.MiddleName.ToLower()));

                if (requestParams.BirthDateFrom != null)
                    query = query.Where(x => x.BuriedPerson.BirthDate >= requestParams.BirthDateFrom);
                if (requestParams.BirthDateTo != null)
                    query = query.Where(x => x.BuriedPerson.BirthDate <= requestParams.BirthDateTo);

                if (requestParams.DeathDateFrom != null)
                    query = query.Where(x => x.BuriedPerson.DeathDate >= requestParams.DeathDateFrom);
                if (requestParams.DeathDateTo != null)
                    query = query.Where(x => x.BuriedPerson.DeathDate >= requestParams.DeathDateTo);

                if (requestParams.BurialDateFrom != null)
                    query = query.Where(x => x.BurialDate >= requestParams.BurialDateFrom);
                if (requestParams.BurialDateTo != null)
                    query = query.Where(x => x.BurialDate >= requestParams.BurialDateTo);

                if (requestParams.CemeteryId != null)
                    query = query.Where(x => x.CemeteryId == requestParams.CemeteryId);
            }
            

            var count = await query.AsNoTracking().CountAsync();

            var burials = await query.Skip((requestParams.PageNumber - 1) * requestParams.PageSize)
                .Take(requestParams.PageSize)
                .Include(x => x.BuriedPerson)
                .Include(x => x.Cemetery)
                .AsNoTracking()
                .ToListAsync();

            var mappedBurials = mapper.Map<IList<BurialDTO>>(burials);

            var pagedResponse = new PagedResponse<BurialDTO>
            {
                TotalCount = count,
                Data = mappedBurials,
                PageNumber = requestParams.PageNumber,
                PageSize = requestParams.PageSize
            };

            return new Response<PagedResponse<BurialDTO>>(pagedResponse, "Dəfnlərin siyahısı");
        }
    }
}
