﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces.Repositories;
using AutoMapper;
using Infrastructure.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public UnitOfWork(ApplicationDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
            Cemeteries = new CemeteryRepository(context, mapper);
            Districts = new DistrictRepository(context, mapper);
            Burials = new BurialRepository(context, mapper);
            Statistics = new StatisticsRepository(context, mapper);
        }

        public ICemeteryRepository Cemeteries { get; private set; }

        public IDistrictRepository Districts { get; private set; }

        public IBurialRepository Burials { get; private set; }

        public IStatisticsRepository Statistics { get; private set; }

        public void Dispose(bool dispose)
        {
            if (dispose)
                context.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task SaveAllAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}
