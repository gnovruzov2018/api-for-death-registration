﻿using ApplicationCore.DTOs;
using ApplicationCore.Interfaces;
using ApplicationCore.Parameters;
using RegistryServiceReference;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class RegistryService : IRegistryService
    {
        private readonly string username = "exidmet";
        private readonly string password = "Ex!dm3t2R";
        BasicHttpBinding httpBinding = null;
        EndpointAddress endpointAddress = null;

        public RegistryService()
        {
            httpBinding = new BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential);
            httpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            endpointAddress = new EndpointAddress(new Uri("https://registryservice.gov.az:10443/RegistryEXidmetService.svc"));
        }
        public async Task GetPersonByPinAsync(PersonByPinRequest request)
        {
            string pin = request.Pin;
            FullPerson person = new FullPerson();
            if (!string.IsNullOrEmpty(pin) && pin.Length > 4 && pin.Length < 8)
            {
                RegistryEXidmetServiceClient client = new RegistryEXidmetServiceClient(httpBinding, endpointAddress);
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;

                SearchPerson search_person = new SearchPerson() { Pin = pin };
                SearchDocument document = new SearchDocument() { Person = search_person };
                SearchParam param = new SearchParam()
                {
                    Document = document,
                    SearchType = SearchType.PersonByPin
                };
                var result = await client.GetOnePersonAsync(param);

                person = result.Person;
                await client.CloseAsync();
            }
        }

        public async Task<PersonFromServiceDTO> GetPersonWithNumber(PersonBySeriesAndNumberRequest request)
        {
            PersonFromServiceDTO person = null;
            if (!string.IsNullOrEmpty(request.Series) && !string.IsNullOrEmpty(request.Number))
            {
                RegistryEXidmetServiceClient client = new RegistryEXidmetServiceClient(httpBinding, endpointAddress);
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;

                SearchParam param = new SearchParam();
                param.SearchType = SearchType.DocumentByNumber;

                SearchDocument document = new SearchDocument();

                if (request.Series == "AA")
                {
                    document.Number = request.Series + request.Number;
                }
                else
                {
                    document.Series = request.Series;
                    document.Number = request.Number;
                }

                param.Document = document;

                FullDocSearchResult result = await client.GetOneDocumentAsync(param);

                client.Close();

                if(result.Document != null)
                {
                    person = new PersonFromServiceDTO
                    {
                        FirstName = result.Document.Person.FirstName,
                        LastName = result.Document.Person.LastName,
                        MiddleName = result.Document.Person.MiddleName,
                        Pin = result.Document.Person.Pin,
                        BirthDate = Convert.ToDateTime(result.Document.Person.DateOfBirthStr),
                        DeathDate = Convert.ToDateTime(result.Document.Person.DateOfDeathStr),
                        Gender = (int)result.Document.Person.Gender
                    };
                }
            }

            return person;
        }
    }
}
