﻿using ApplicationCore.DTOs.Account;
using ApplicationCore.Entities;
using ApplicationCore.Exceptions;
using ApplicationCore.Interfaces;
using ApplicationCore.Settings;
using ApplicationCore.Wrappers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUserService userService;
        private readonly JWTSettings jwtSettings;

        public AccountService(IUserService userService,
            IOptions<JWTSettings> jwtSettings)
        {
            this.userService = userService;
            this.jwtSettings = jwtSettings.Value;
        }

        public async Task<Response<AuthenticationResponse>> AuthenticateAsync(AuthenticationRequest request, string ipAddress)
        {
            var userExists = await userService.UserExistsAsync(request.UserName);
            if (!userExists)
                throw new ApiException($"'{request.UserName}' istifadəçi adı ilə hesab mövcud deyil");

            var user = await userService
                .GetOne<User>(x => x.UserName == request.UserName && x.Password == request.Password, x => x.Role);

            if(user == null)
                throw new ApiException($"'{request.UserName}' istifadəçi adı üçün daxil etdiyiniz şifrə yanlışdır");

            JwtSecurityToken jwtSecurityToken = GenerateJWToken(user);
            AuthenticationResponse response = new AuthenticationResponse();
            response.UserId = user.UserId;
            response.JWToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
            response.Email = user.Email;
            response.UserName = user.UserName;
            response.Role = user.Role.RoleName;
            response.CemeteryId = user.CemeteryId;
            response.DistrictId = user.DistrictId;
            var refreshToken = GenerateRefreshToken(ipAddress, user.UserId);
            await userService.AddUserRelatedEntity<RefreshToken>(refreshToken);
            response.RefreshToken = refreshToken.Token;
            return new Response<AuthenticationResponse>(response, $"Uğurlu giriş");
        }

        public async Task<Response<AuthenticationResponse>> RefreshToken(string token, string ipAddress)
        {
            var refreshTokenFromDb = await userService.GetOne<RefreshToken>(x => x.Token == token);

            // return null if no user found with token
            if (refreshTokenFromDb == null || !refreshTokenFromDb.IsActive)
                throw new ApiException($"'Refresh token' ya yanlışdır ya da aktiv deyil");

            // get user by id
            var user = await userService.GetOne<User>(x => x.UserId == refreshTokenFromDb.UserId, x => x.Role);

            // replace old refresh token with a new one and save
            var newRefreshToken = GenerateRefreshToken(ipAddress, user.UserId);
            refreshTokenFromDb.Revoked = DateTime.Now;
            refreshTokenFromDb.RevokedByIp = ipAddress;
            refreshTokenFromDb.ReplacedByToken = newRefreshToken.Token;
            await userService.AddUserRelatedEntity<RefreshToken>(newRefreshToken);
            await userService.UpdateAsync<RefreshToken>(refreshTokenFromDb);

            // generate new jwt
            
            var jwtToken = GenerateJWToken(user);

            var response = new AuthenticationResponse();
            response.UserId = user.UserId;
            response.JWToken = new JwtSecurityTokenHandler().WriteToken(jwtToken);
            response.Email = user.Email;
            response.UserName = user.UserName;
            response.Role = user.Role.RoleName;
            response.RefreshToken = newRefreshToken.Token;
            response.CemeteryId = user.CemeteryId;
            response.DistrictId = user.DistrictId;
            return new Response<AuthenticationResponse>(response, $"Access token uğurla yeniləndi");
        }

        public async Task<Response<object>> RevokeToken(string token, string ipAddress)
        {
            if(string.IsNullOrEmpty(token))
                throw new ApiException($"'Refresh token' boş ola bilməz");

            var refreshTokenFromDb = await userService.GetOne<RefreshToken>(x => x.Token == token);

            // return null if no user found with token
            if (refreshTokenFromDb == null || !refreshTokenFromDb.IsActive)
                throw new ApiException($"'Refresh token' ya yanlışdır ya da aktiv deyil");

            // revoke token and save
            refreshTokenFromDb.Revoked = DateTime.Now;
            refreshTokenFromDb.RevokedByIp = ipAddress;
            await userService.UpdateAsync<RefreshToken>(refreshTokenFromDb);

            return new Response<object>(new { Message = "'Refresh token' uğurla ləğv edildi" }, $"'Refresh token' uğurla ləğv edildi");
        }

        private JwtSecurityToken GenerateJWToken(User user)
        {

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim("uid", user.UserId.ToString()),
                new Claim("role", user.Role.RoleName)
            };

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Key));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha512Signature);

            var jwtSecurityToken = new JwtSecurityToken(
                issuer: jwtSettings.Issuer,
                audience: jwtSettings.Audience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(jwtSettings.DurationInMinutes),
                signingCredentials: signingCredentials);
            return jwtSecurityToken;
        }

        private RefreshToken GenerateRefreshToken(string ipAddress, int userId)
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = DateTime.Now.AddDays(7),
                    Created = DateTime.Now,
                    CreatedByIp = ipAddress,
                    UserId = userId
                };
            }
        }


    }
}
